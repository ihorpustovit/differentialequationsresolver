package com.smbr;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ResultTableData {
    private List<RowData> rows = new ArrayList<>();

    public void addRow(RowData row){
        rows.add(row);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        rows.forEach(row -> stringBuilder.append(row.toString()));
        return stringBuilder.toString();
    }
}
