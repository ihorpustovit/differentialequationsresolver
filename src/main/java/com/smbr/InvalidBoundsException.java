package com.smbr;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidBoundsException extends RuntimeException {
    public InvalidBoundsException(String message) {
        super(message);
    }
}
