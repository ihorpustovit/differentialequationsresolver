package com.smbr;

public interface DifferentialEquationResolver {
    ResultTableData resolve(DifferentialEquationRequest request);
}
