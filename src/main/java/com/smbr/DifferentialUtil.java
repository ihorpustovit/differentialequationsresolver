package com.smbr;

public class DifferentialUtil {
    public static DifferentialEquationRequest testRequest(){
        DifferentialEquationRequest request = new DifferentialEquationRequest();

        request.setBound(new DifferentialEquationRequest.Bound(0,1));
        request.setGrowthRate(0.1);
        request.setY(1.0);
        request.setFunction((x,y) -> Math.pow(x,2) - 2 * y);

        return request;
    }

    public static DifferentialEquationRequest labRequest(){
        DifferentialEquationRequest request = new DifferentialEquationRequest();

        request.setBound(new DifferentialEquationRequest.Bound(0,1));
        request.setGrowthRate(0.1);
        request.setY(0.0);
        request.setFunction((x,y) -> Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.sin(x + 2 * y)));

        return request;
    }
}
