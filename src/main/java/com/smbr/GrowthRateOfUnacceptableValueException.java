package com.smbr;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GrowthRateOfUnacceptableValueException extends RuntimeException{
    public GrowthRateOfUnacceptableValueException(String msg){
        super(msg);
    }
}
