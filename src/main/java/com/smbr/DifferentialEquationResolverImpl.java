package com.smbr;

import java.util.function.BinaryOperator;

import static com.smbr.DifferentialEquationRequest.*;
import static com.smbr.RowData.*;

public class DifferentialEquationResolverImpl implements DifferentialEquationResolver{

    @Override
    public ResultTableData resolve(DifferentialEquationRequest request) {
        ResultTableData tableData = new ResultTableData();
        BinaryOperator<Double> targetFunction = request.getFunction();
        double y = request.getY();
        int iteration = 0;

        for (double x = request.getBound().getLowerBound(); x <= request.getBound().getUpperBound(); x += request.getGrowthRate()){
            RowData rowData = new RowData();
            double functionVal = targetFunction.apply(x,y);

            rowData.setPointNumber(iteration);
            rowData.setPoint(new Point(x,y));
            rowData.setFunctionValue(functionVal);
            tableData.addRow(rowData);
            y = y + functionVal * request.getGrowthRate();
            iteration++;
        }

        return tableData;
    }

    private void validateGrowthRateOf(DifferentialEquationRequest request){
        Bound requestBound = request.getBound();

        if (request.getGrowthRate() >= (requestBound.getUpperBound() - requestBound.getLowerBound())){
            throw new GrowthRateOfUnacceptableValueException("Growth rate shouldn't be greater or equal to the bound size");
        }
    }
}
