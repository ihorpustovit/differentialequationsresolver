package com.smbr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RowData {
    private int pointNumber;
    private Point point;
    private double functionValue;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Point{
        private double x;
        private double y;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("| ")
                .append(pointNumber)
                .append(" | ")
                .append(String.format("%1$,.5f",point.x))
                .append(" | ")
                .append(String.format("%1$,.5f",point.y))
                .append(" | ")
                .append(String.format("%1$,.5f",functionValue))
                .append(" |")
                .append("\n");
        return sb.toString();
    }
}
