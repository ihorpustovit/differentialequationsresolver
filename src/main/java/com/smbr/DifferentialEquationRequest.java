package com.smbr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.function.BinaryOperator;

@Getter
@Setter
public class DifferentialEquationRequest {
    private BinaryOperator<Double> function;
    private Double y;
    private Bound bound;
    private double growthRate;

    @Getter
    @AllArgsConstructor
    public static class Bound{
        private double lowerBound;
        private double upperBound;

        public void setLowerBound(double lowerBound) {
            if (upperBound > lowerBound){
                this.lowerBound = lowerBound;
            } else {
                throw new InvalidBoundsException("Lower bound shouldn't be greater or equal to upper bound");
            }
        }

        public void setUpperBound(double upperBound) {
            if (lowerBound < upperBound){
                this.upperBound = upperBound;
            } else {
                throw new InvalidBoundsException("Upper bound shouldn't be lower or equal to lower bound");
            }
        }
    }
}
